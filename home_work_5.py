## Создайте массив состоящий из словарей с данными
set =[
    {
        "name": "Jack",
        "age": 45
    },
    {
        "name": "Daniels",
        "age": 18
    },
    {
        "name": "XO",
        "age": 15
    }
]


# Напишите функцию которая принимает ранее созданный массив, фильтрирует
# полученный массив и возвращающий не менне двух элементов из массива
def filtr(smth):
    l = []
    for frukt in smth:
        if frukt["age"] >= 18:
            l.append(frukt)
    return l


filtr_nums = filtr(set)
print(filtr_nums)


# Напишите функцию которая принимает отфильтрованные данные, добавляет
# новое значение каждому из элементов отфильтрованных данных и возвращает
# измененные данные с добавленными значениями
def add(a):
    for i in a:
        i["alhogol"] = True

    return a


addedMass = add(filtr_nums)
print(addedMass)

# Напишите функцию которая принимает массив ранее измененых данных,
# меняет значение в каждом из элементов и возращает измененные данные


def change(arr):
    for i in arr:
        i["age"] += 1

    return arr


changedMass = change(addedMass)
print(changedMass)


# Напишите функцию которая принимает массив ранее измененых данных,
# и поочередно выводит их в консоль

def printer(arr):
    for i in arr:
        print(i)


printer(changedMass)
